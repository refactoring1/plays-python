import decimal
from math import floor
from babel.numbers import format_currency


def statement(invoice, plays):
    total_amount = 0
    volume_credits = 0
    result = "Statement for " + invoice["customer"] + "\n"
    for perf in invoice["performances"]:
        play = plays[perf["playID"]]
        if play["type"] == "tragedy":
            this_amount = 40000
            if perf["audience"] > 30:
                this_amount += 1000 * (perf["audience"] - 30)
        elif play["type"] == "comedy":
            this_amount = 30000
            if perf["audience"] > 20:
                this_amount += 10000 + 500 * (perf["audience"] - 20)
            this_amount += 300 * perf["audience"]
        else:
            raise RuntimeError("unknown type: " + play["type"])
        # add volume credits
        volume_credits += max(perf["audience"]-30, 0)
        # add extra credit for every ten comedy attendees
        if play["type"] == "comedy":
            volume_credits += floor(perf["audience"] / 5)
        result += "    {0}: {1} ({2} seats)\n".format(
            play["name"],
            format_currency(decimal.Decimal(this_amount / 100), "USD", locale="en_US"),
            perf["audience"]
        )
        total_amount += this_amount
    # print line for this order
    result += "Amount owed is " + format_currency(decimal.Decimal(total_amount / 100), "USD", locale="en_US") + "\n"
    result += "You earned {} credits\n".format(volume_credits)
    return result
