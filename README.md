# Plays Refactoring Kata

## Setup

### How to setup virtual environment

*nix
```shell 
virtualenv venv
```
or
```shell
python -m venv venv
```

### How to activate venv & install deps  
```
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt
``` 

```shell
docker build -t plays .
docker run -v "$(pwd):/usr/src/app" -it plays /bin/bash
```


## Steps

### Phase 1: Extract functions

1. Извлечь play_for 
   1. за-inline-ить по месту применения
1. Извлечь if - elif - else в отдельный метод amount_for(...)
   1. за-inline-ить по месту применения
1. Выделить метод по форматированию валюты
1. Извлечь метод volume_credits_for(...)
1. Разбить цикл
1. Выделить метод total_volume_credits()
1. Разбить цикл
1. Выделить метод total_amount()

### Phase 2: Prepare statement data
1. Извлечь метод render_plain_text()
1. Подготовить данные для отчёта в отдельной функции statement_data()
1. Перенести все расчёты в отдельный файл
1. Создать функцию render_html()

### Phase 3: Polymorphic performance

1. Создать класс Performance() который будет работать с данными по представлению
1. Перенести логику amount_for() в Performance amount()
1. Создать классы-наследники Tragedy() и Comedy() 
1. Перенести специфичную для каждого из классов логику в amount()
1. Повторить для volume_credits_for